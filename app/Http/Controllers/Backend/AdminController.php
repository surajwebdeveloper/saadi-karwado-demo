<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $userlist = User::where('id', '!=',$user->id)->get();
        return view('Backend.User.index',compact('userlist'));
    }

    public function filter(Request $request)
    {
        $incomerange = explode("-",$request['income_range']);
        $from = str_replace("$","",$incomerange[0]);
        $to = str_replace("$","",$incomerange[1]);
        $userlist = User::where('gender',$request->gender)->Where('family_type',$request->family_type)
                    ->where('manglik',$request['manglik'])
                    ->where('annual_income','>=',(int)$from)
                    ->where('annual_income','<=',(int)$to)->get();
        return view('Backend.User.index',compact('userlist'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
