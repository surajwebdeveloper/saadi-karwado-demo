<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Partner;
use Auth;

class UserController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $userlist = User::where('id', '!=',$user->id)->get();
        $partner = Partner::where('user_id',$user->id)->first();
        return view('Backend.User.matching',compact('userlist','partner'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
