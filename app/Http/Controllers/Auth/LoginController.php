<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use Auth;
use Redirect;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }
    public function Login(Request $request){
        $data = $request->all();

        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $checkLogin = User::where('email', $request['email'])->first();
        if (empty($checkLogin)) {
            return redirect()->back()
                ->withErrors(['email' => "User not found.!"]);
        }

        $logindetails = array(
            'email' => $request['email'],
            'password' => $request['password']
        );

        
        if (Auth::attempt($logindetails)) 
        {
            if (Auth::user()->hasRole('Admin'))
            {
               return redirect('userlist');
            }
            elseif(Auth::user()->hasRole('User'))
            {
                return redirect('matching');
            }
            
            Auth::logout();
            return redirect()->back()
                ->withErrors(['email' => "Invalid Login Details."]);

        } else {
            Auth::logout();
            return redirect()->back()
                ->withErrors(['email' => 'Invalid Login Details.']);
        }
    }

}
