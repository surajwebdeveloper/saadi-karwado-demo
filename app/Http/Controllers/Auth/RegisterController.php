<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Partner;
use Session;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }
    public function Register(Request $request){
        
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'annual_income' => 'required',
        ]);
        
        $user = new User();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->date_of_birth = $request['date_of_birth'];
        $user->gender = $request['gender'];
        $user->annual_income = $request['annual_income'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->occupation = $request['occupation'];
        $user->family_type = $request['family_type'];
        $user->manglik = $request['manglik'];
        if($user->save())
        {
            //role assign to user
            $user->assignRole(2);
            $partner = new Partner();
            $partner->user_id = $user->id;
            $expected_income = explode("-",$request['expected_income']);
            $expectedsallery = str_replace("$","",$expected_income[0]);
            $partner->expected_income = $expectedsallery;
            $partner->occupation = isset($request['partneroccupation']) ? implode(",",$request['partneroccupation']) : "";
            $partner->family_type = isset($request['partnerfamilytype']) ? implode(",",$request['partnerfamilytype']) : "";
            $partner->manglik = $request['partnermanglik'];
            $partner->save();
            Session::flash('message','<div class="alert alert-success"><strong>Success!</strong>Registration successfully.</div>');
        }else{
            Session::flash('message','<div class="alert alert-danger"><strong>Success!</strong>Registration failled.</div>');
        }
        return redirect()->to('/');
        
    }
}
