<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = 'partner_preference';
    protected $fillable = ['user_id','expected_income','occupation','family_type','manglik','created_at','updated_at'];
}
