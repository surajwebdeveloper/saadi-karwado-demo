<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Saadi Karwado-Backend</title>
        @include('includes.backend.head')
        @yield('css')
       
    </head>

    <body data-sidebar="dark" >
       <!-- ======Ajax Loader=== -->
       <div id="overlay-1">
                <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        <!-- =======End Ajax Loader -->
        <!-- Begin page -->
        <div id="layout-wrapper">

            @include('includes.backend.header')
            <!-- ========== Left Sidebar Start ========== -->
            @include('includes.backend.sidebar')
        <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            @yield('content')
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        @include('includes.backend.script')

        @yield('js')
<script> 

//   jQuery(function($)
//   {
//       $(document).ajaxSend(function()
//       {
//           $("#overlay-1").fadeIn(300);　
//       });

//   $(document).ajaxComplete(function()
//   {
//          setTimeout(function()
//          {
//                $("#overlay-1").fadeOut(300);
//               },500);　
//         });
//   });
    </script> 
    </body>
</html>
