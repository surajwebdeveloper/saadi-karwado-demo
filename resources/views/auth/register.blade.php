<!-- Developer : Suraj Gupta
Start Date : 24-09-2022 01:25 PM -->
<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Register | Saadi Karwado</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap Css -->
        <link href="{{URL::to('storage/app/public/admin/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{URL::to('storage/app/public/admin/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{URL::to('storage/app/public/admin/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
</head>

    <body>
        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="bg-soft-primary">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="text-primary p-4">
                                             <h5 class="text-primary">Welcome </h5>
                                            <p>Sign Up to <b>Saadi Karwado</b>.</p>
                                        </div>
                                    </div>
                                    <div class="col-5 align-self-end">
                                        <img src="{{URL::to('storage/app/public/admin/images/profile-img.png') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0"> 
                                <div class="p-2">
                                    <form class="form-horizontal" action="{{URL::to('Register')}}" method="post">
                                    @if(!empty($errors->all()))
                                    @foreach($errors->all() as $error)
                                    <div class="alert alert-card alert-danger" role="alert">
                                        {{ $error }}
                                    </div>
                                    @endforeach
                                    @endif
                                   
                                    @csrf
                                        <div class="form-group">
                                            <label for="first_name">First Name</label><span style="color: red;">*</span>
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label><span style="color: red;">*</span>
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label><span style="color: red;">*</span>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="userpassword">Password</label><span style="color: red;">*</span>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" maxlength="8">
                                        </div>
                                        <div class="form-group">
                                            <label for="dob">DOB</label><span style="color: red;">*</span>
                                            <input type="date" class="form-control" id="date_of_birth" name="date_of_birth">
                                        </div>
                                        <div class="form-group">
                                            <label for="dob">Gender </label><span style="color: red;">*</span><br>
                                            <label for="male">Male</label>
                                            <input type="radio" id="male" name="gender" value="Male" checked>
                                            <label for="female">Female</label>
                                            <input type="radio" id="female" name="gender" value="Female">
                                        </div>
                                        <div class="form-group">
                                            <label for="annualincome">Annual Income In($)</label><span style="color: red;">*</span>
                                            <input type="number" class="form-control" id="annual_income" name="annual_income">
                                        </div>
                                        <div class="form-group">
                                            <label for="occupation">Occupation</label>
                                            <select name="occupation" id="occupation" class="form-control">
                                                <option value="">---Select Occupation---</option>
                                                <option value="Private Job">Private Job</option>
                                                <option value="Government Job">Government Job</option>
                                                <option value="Bussiness">Bussiness</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="family_type">Family Type</label>
                                            <select name="family_type" id="family_type" class="form-control">
                                                <option value="">---Select Family---</option>
                                                <option value="Joint Family">Joint Family</option>
                                                <option value="Nuclear">Nuclear Family</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="manglik">Manglik</label>
                                            <select name="manglik" id="manglik" class="form-control">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="text-primary">
                                             <h3 class="text-primary">Partner Preference </h3>
                                        </div>
                                        <hr>

                                        <div class="form-group">
                                            <label for="amount">Expected Income</label>
                                            <input type="text" id="amount" name="expected_income" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                            <div id="slider-range"></div>
                                        </div>
                                        

                                        <div class="form-group">
                                            <label for="partneroccupation">Occupation</label>
                                            <select name="partneroccupation[]" id="partneroccupation" multiple class="form-control">
                                                <option value="Private Job">Private Job</option>
                                                <option value="Government Job">Government Job</option>
                                                <option value="Bussiness">Bussiness</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="partnerfamilytype">Family Type</label>
                                            <select name="partnerfamilytype[]" id="partnerfamilytype" multiple class="form-control">
                                                <option value="Joint Family">Joint Family</option>
                                                <option value="Nuclear">Nuclear Family</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="partnermanglik">Manglik</label>
                                            <select name="partnermanglik" id="partnermanglik" class="form-control">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="Both">Both</option>
                                            </select>
                                        </div>
                                        
                                        <div class="mt-3">
                                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Sign Up</button>
                                        </div>
                                        <center class="mt-3">You have an account? <a href="{{URL::to('/')}}">Log In</a></center>
                                    </form>
                                </div>
            
                            </div>
                        </div>
                       

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 99999999,
      values: [ 10000000, 99999999 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
      
  } );
  </script>
