<!-- Developer : Suraj Gupta
Start Date : 25-09-2022 08:25 AM -->
<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Login | Saadi Karwado</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap Css -->
        <link href="{{URL::to('storage/app/public/admin/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{URL::to('storage/app/public/admin/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{URL::to('storage/app/public/admin/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />
</head>

    <body>
        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="bg-soft-primary">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="text-primary p-4">
                                            <h5 class="text-primary">Welcome Back !</h5>
                                            <p>Sign in to <b>Saadi Karwado</b>.</p>
                                        </div>
                                    </div>
                                    <div class="col-5 align-self-end">
                                        <img src="{{URL::to('storage/app/public/admin/images/profile-img.png') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0"> 
                                <div class="p-2">
                                    <form class="form-horizontal" action="{{URL::to('login')}}" method="post">
                                    @if(!empty($errors->all()))
                                    @foreach($errors->all() as $error)
                                    <div class="alert alert-card alert-danger" role="alert">
                                        {{ $error }}
                                    </div>
                                    @endforeach
                                    @endif
                                    <div class="col-md-12">
                                    @if(Session::has('message'))
                                    {!! Session::get('message') !!}
                                    @endif
                                    </div>
                                    @csrf
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email">
                                        </div>
                
                                        <div class="form-group">
                                            <label for="userpassword">Password</label>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                                        </div>
                                        
                                        <div class="mt-3">
                                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                                        </div>
                                        <center class="mt-3">Don't have an account? <a href="{{URL::to('register')}}">User SIGN UP</a></center>
                                    </form>
                                </div>
            
                            </div>
                        </div>
                       

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
