@extends('layouts.backend')
@section('css')
<!-- DataTables -->
<link href="{{URL::to('storage/app/public/admin/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}"
   rel="stylesheet" type="text/css" />
<link href="{{URL::to('storage/app/public/admin/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}"
   rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{URL::to('storage/app/public/admin/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .importantRule { display: none !important; }
</style>
@endsection
@section('content')
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="mb-0 font-size-18">Suggestion List</h4>
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Suggestion List</a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="col-md-12 px-0">
            @if(Session::has('message'))
            {!! Session::get('message') !!}
            @endif
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                        <h4 class="card-title">Users</h4>
                       
                     <table id="datatable" class="table table-bordered dt-responsive nowrap custom-data-tbl"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;" data-order='[[ 6, "desc" ]]'>
                        <thead>
                           <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>DOB</th>
                              <th>Gender</th>
                              <th>Annual Income</th>
                              <th>Manglik</th>
                              <th>Matching In %</th>
                           </tr>
                        </thead>
                        <tbody id="tblbody">
                          @php $no = 0;
                          $po = explode(",",$partner->occupation);
                          $pf = explode(",",$partner->family_type);
                          $pm = explode(",",$partner->manglik);
                          @endphp
                          @forelse($userlist as $user)
                          @php $perocc = 0 @endphp
                          @if (in_array($user->occupation, $po))
                               @php $perocc += 25 @endphp
                                @else
                                @php $perocc += 0 @endphp
                                @endif
                                @if (in_array($user->family_type, $pf))
                                @php $perocc += 25 @endphp
                                @else
                                @php $perocc += 0 @endphp
                                @endif
                                @if (in_array($user->manglik, $pm))
                                @php $perocc += 25 @endphp
                                @else
                                @php $perocc += 0 @endphp
                                @endif
                                @if($partner->manglik >= $user->annual_income)
                                @php $perocc += 25 @endphp
                                @else
                                @php $perocc += 0 @endphp
                                @endif
                          <tr>
                             <td>{{++$no}}</td>
                             <td>{{$user->first_name." ".$user->last_name}}</td>
                             <td>{{$user->date_of_birth}}</td>
                             <td>{{$user->gender}}</td>
                             <td>${{$user->annual_income}}</td>
                             <td>{{$user->manglik}}</td>
                            <td><span class="btn btn-success">{{$perocc}}%</span></td>
                          </tr>
                          @empty
                          @endforelse
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <!-- end col -->
         </div>
         <!-- end row -->
      </div>
      <!-- container-fluid -->
   </div>
   <!-- End Page-content -->
   @include('includes.backend.footer')
</div>
@endsection
@section('js')
<!-- Required datatable js -->
<script src="{{URL::to('storage/app/public/admin/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('storage/app/public/admin/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{URL::to('storage/app/public/admin/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script
   src="{{URL::to('storage/app/public/admin/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('storage/app/public/admin/js/pages/datatables.init.js')}}"></script>
@endsection