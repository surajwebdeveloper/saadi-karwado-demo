@extends('layouts.backend')
@section('css')
<!-- DataTables -->
<link href="{{URL::to('storage/app/public/admin/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}"
   rel="stylesheet" type="text/css" />
<link href="{{URL::to('storage/app/public/admin/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}"
   rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{URL::to('storage/app/public/admin/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- ==== Toaster ==== -->
<link rel="stylesheet" href="{{URL::to('storage/app/public/admin/toastr/toastr.min.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
<style type="text/css">
  .importantRule { display: none !important; }
</style>
@endsection
@section('content')
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="mb-0 font-size-18">User List</h4>
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">User List</a></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="col-md-12 px-0">
            @if(Session::has('message'))
            {!! Session::get('message') !!}
            @endif
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                        <h4 class="card-title">Users</h4>

            {{Form::open(array('route'=>'filter','method'=>'POST','class'=>'needs-validation'))}}
                <div class="row col-md-12">
                  <div class="col-md-3">
                    <label>Gender</label>
                    <select name="gender" class="form-control">
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Family Type</label>
                    <select name="family_type" class="form-control">
                      <option value="Joint Family">Joint Family</option>
                      <option value="Nuclear Family">Nuclear Family</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Manglik</label>
                    <select name="manglik" class="form-control">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                        <label for="amount">Income Range</label>
                        <input type="text" id="amount" name="income_range" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        <div id="slider-range"></div>
                    </div>
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-info" style="margin-top: 27px;" name="Filter" value="Filter">
                  </div>
              </div>
              {{Form::close()}}
              <br>
              <br>
                       
                     <table id="datatable" class="table table-bordered dt-responsive nowrap custom-data-tbl"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                           <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>DOB</th>
                              <th>Gender</th>
                              <th>Annual Income</th>
                              <th>Manglik</th>
                              <th>Family Type</th>
                           </tr>
                        </thead>
                        <tbody id="tblbody">
                          @php $no = 0; @endphp
                          @forelse($userlist as $user)
                          <tr>
                             <td>{{++$no}}</td>
                             <td>{{$user->first_name." ".$user->last_name}}</td>
                             <td>{{$user->date_of_birth}}</td>
                             <td>{{$user->gender}}</td>
                             <td>${{$user->annual_income}}</td>
                             <td>{{$user->manglik}}</td>
                             <td>{{$user->family_type}}</td>
                          </tr>
                          @empty
                          @endforelse
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <!-- end col -->
         </div>
         <!-- end row -->
      </div>
      <!-- container-fluid -->
   </div>
   <!-- End Page-content -->
   @include('includes.backend.footer')
</div>
@endsection
@section('js')
<!-- Required datatable js -->
<script src="{{URL::to('storage/app/public/admin/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('storage/app/public/admin/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{URL::to('storage/app/public/admin/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script
   src="{{URL::to('storage/app/public/admin/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('storage/app/public/admin/js/pages/datatables.init.js')}}"></script>

<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script type="text/javascript">


  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 99999999,
      values: [ 1, 99999999 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
      
  } );
  

</script>
@endsection