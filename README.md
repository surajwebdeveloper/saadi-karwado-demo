# Saadi Karwado Demo 



## Getting started

For Setup Please Follow This Step
=========================================
Clone Project From Gitlab by using this link :
https://gitlab.com/surajwebdeveloper/saadi-karwado-demo.git

Create Database in mysql with name suraj

After Clone What Do Next please follow below step on command promt
====================================================

1) composer update
2) php artisan migrate 
3) php artisan db:seed --class=CreateRoleSeeder  => for Role Create
4) php artisan db:seed --class=CreateDummyUsers  => for Dummy Users
5) php artisan db:seed --class=CreateAdminUser   => for Admin User