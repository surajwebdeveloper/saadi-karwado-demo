<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Auth'],function()
{
    Route::get('/','LoginController@index');
    Route::get('/register','RegisterController@index');
    Route::post('login','LoginController@Login');
    Route::post('Register','RegisterController@Register');
});

Route::group(['namespace'=>'Backend'],function()
{
    Route::group(['middleware' => ['BackendAuth']],function()
    {
        Route::get('/userlist','AdminController@index');
        Route::post('/filter','AdminController@filter')->name('filter');
        Route::get('/logout','AdminController@logout');
    });
    Route::group(['middleware' => ['UserAuth']],function()
    {
        Route::get('/matching','UserController@index');
        Route::get('/logout','UserController@logout');
    });
    
});
