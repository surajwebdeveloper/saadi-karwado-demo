<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Suraj',
            'last_name' => 'Gupta',
            'email' => 'admin'.mt_rand(100, 999).'@gmail.com',
            'password' => bcrypt('123456'),
            'date_of_birth' => now(),
            'gender' => 'Male',
            'annual_income' => mt_rand(100000, 999999),
        ]);

        $user->assignRole(1);
    }
}
