<?php

use Illuminate\Database\Seeder;
use App\User;
Use App\Partner;

class CreateDummyUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {

            $occupation = collect(['Private job', 'Government Job', 'Business']);
            $shuffled = $occupation->shuffle();
            $family_type = collect(['Joint family', 'Nuclear family']);
            $shuffled1 = $family_type->shuffle();
            $manglik = collect(['Yes', 'No']);
            $shuffled2 = $manglik->shuffle();
            $gender = collect(['Male', 'Female']);
            $gen = $gender->shuffle();
            $user = User::create([
                'first_name' => 'USer'.$i,
                'last_name' => 'Last'.$i,
                'date_of_birth' => now(),
                'gender' => $gen[0],
                'annual_income' => mt_rand(10000000, 99999999),
                'email' => 'user'.mt_rand(1000, 9999).'@gmail.com',
                'password' => bcrypt('123456'),
                'occupation' =>  $shuffled[0],
                'family_type' => $shuffled1[0],
                'manglik' => $shuffled2[0]
            ]);
            $user->assignRole(2);
            $occupation1 = array('Private job', 'Government Job', 'Business');
            $shuffled3 = array_rand($occupation1,2);
            $family_type1 = array('Joint family', 'Nuclear family');
            $shuffled4 = array_rand($family_type1,1);
            $manglik1 = collect(['Yes', 'No','Both']);
            $shuffled5 = $manglik1->shuffle();
            $partner = Partner::create([
                'user_id' => $user->id,
                'expected_income' => mt_rand(10000000, 99999999),
                'occupation'=> $occupation1[$shuffled3[0]].",".$occupation1[$shuffled3[1]],
                'family_type' => $family_type1[$shuffled4],
                'manglik' => $shuffled5[0],
            ]);
        }
    }
}
